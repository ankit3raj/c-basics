#include <iostream>

using namespace std;

int main() {
	int N;
	cin>>N;
	for(int row=1;row<=N;row++){
		for(int col=1;col<=row;col++){
			if(row%2!=0){
				cout<<"1";
			}
			else{
				if(col==1 || col==row){
					cout<<"1";
				}
				else{
					cout<<"0";
				}
			}
		}
		cout<<""<<endl;
	}
}